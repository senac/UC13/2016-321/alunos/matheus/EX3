package br.com.senac.ex3;

public class ConverterMoedas {

    public double Converter(Moeda moeda) {

        switch (moeda.converterPara) {
            case EUR:
                return moeda.getValor() / 3.83;
            case USD:
                return moeda.getValor() / 3.23;
            case dolarAustraliano:
                return moeda.getValor() / 2.46;
            default:
                return moeda.getValor();
        }

    }

}
