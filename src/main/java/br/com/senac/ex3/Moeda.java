package br.com.senac.ex3;

public class Moeda {

    private double valor;
    protected ConverterPara converterPara;

    public Moeda() {
    }

    public Moeda(double valor, ConverterPara converterPara) {
        this.valor = valor;
        this.converterPara = converterPara;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public ConverterPara getConverterPara() {
        return converterPara;
    }

    public void setConverterPara(ConverterPara converterPara) {
        this.converterPara = converterPara;
    }

}
