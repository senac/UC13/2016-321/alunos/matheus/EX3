package br.com.senac.ex3;

import junit.framework.Assert;
import org.junit.Test;

public class ConverterMoedasTest {

    @Test
    public void deveConveterDezMilReaisParaEuro() {

        ConverterMoedas converterMoedas = new ConverterMoedas();
        Moeda moeda = new Moeda(10000, ConverterPara.EUR);
        double valorFinal = converterMoedas.Converter(moeda);
        Assert.assertEquals(10000 / 3.83, valorFinal, 0.01);
    }

    @Test
    public void deveConveterDezMilReaisParaDolarAmericano() {

        ConverterMoedas converterMoedas = new ConverterMoedas();
        Moeda moeda = new Moeda(10000, ConverterPara.USD);
        double valorFinal = converterMoedas.Converter(moeda);
        Assert.assertEquals(10000 / 3.23, valorFinal, 0.01);
    }

    @Test
    public void deveConveterDezMilReaisParaDolarAustraliano() {

        ConverterMoedas converterMoedas = new ConverterMoedas();
        Moeda moeda = new Moeda(10000, ConverterPara.dolarAustraliano);
        double valorFinal = converterMoedas.Converter(moeda);
        Assert.assertEquals(10000 / 2.46, valorFinal, 0.01);
    }

}
